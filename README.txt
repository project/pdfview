A module to generate a pdf files from nodes.

It relies on TCPDF, which is a free PHP class.
It can be found here: http://tcpdf.sourceforge.net

The TCPDF distribution comes with instructions for handling custom fonts and
encodings. Please refer to their documentation before posting a feature or
support requestion on http://drupal.org
